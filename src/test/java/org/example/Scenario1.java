package org.example;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.Iterator;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class Scenario1 extends BaseForTests {
    public void clickLinkByHref(String href) {
        List<WebElement> anchors = driver.findElements(By.tagName("a"));
        Iterator<WebElement> i = anchors.iterator();

        while(i.hasNext()) {
            WebElement anchor = i.next();
            if(anchor.getAttribute("href").contains(href)) {
                anchor.click();
                break;
            }
        }
    }
    @Test
    void scenario1() {
        login("Lovelace","lovelace");
        assertTrue(driver.getTitle().contains("Page des TER de M1"));
        clickLinkByHref("/teacher/list");
        clickLinkByHref("/teacher/add");
        WebElement  error= driver.findElement(By.tagName("h2"));
        String errorText=read(error);
        assertTrue(errorText.contains("Something went wrong"));
    }
}
