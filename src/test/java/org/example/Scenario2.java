package org.example;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class Scenario2 extends BaseForTests {
    public void clickLinkByHref(String href) {
        List<WebElement> anchors = driver.findElements(By.tagName("a"));
        Iterator<WebElement> i = anchors.iterator();

        while(i.hasNext()) {
            WebElement anchor = i.next();
            if(anchor.getAttribute("href").contains(href)) {
                anchor.click();
                break;
            }
        }
    }

    @Test
    void scenario2() throws IOException {
        login("Chef", "mdp");
        assertTrue(driver.getTitle().contains("Page des TER de M1"));
        clickLinkByHref("/teacher/list");
        clickLinkByHref("/teacher/add");
        WebElement firstName = driver.findElement(By.id("firstName"));
        WebElement lastName = driver.findElement(By.id("lastName"));
        write(firstName,"TeacherfirstnameforTest");
        write(lastName,"TeacherLastNameForTest");
        WebElement createButton = driver.findElement(By.cssSelector("[type=submit]"));
        click(createButton);
        screenshot("teacher_screenshot");
    }
}
